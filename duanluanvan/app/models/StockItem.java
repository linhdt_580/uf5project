package models;
import models.Student;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class StockItem extends Model{
    @Id
    public Long id;
    @ManyToOne
    public Student student;               
    public Long quantity;

    public String toString() {
        return String.format("StockItem %d - %d x student %s", id, quantity,student == null ? null : student.id);
    }
    public static Finder<Long, StockItem> find =
            new Finder<>(Long.class, StockItem.class);
    public static StockItem findById(Long id) {
        return find.byId(id);
    }
}
