package controllers;

import models.*;
import play.mvc.*;
import play.data.Form;
import play.data.validation.Constraints;
import java.util.*;
import views.html.*;
import views.html.students.*;
import com.avaje.ebean.Ebean;
import models.Student;
import models.UserAccount;
import models.StockItem;
import com.avaje.ebean.Page;
import java.lang.String.*;


@Security.Authenticated(Secured.class)
public class Students extends Controller
{

    private static final Form<Student> studentForm = Form.form(Student.class);

    public static Result list(Integer page) {
        Page<Student> students = Student.find(page);
        return ok(views.html.catalog.render(students));
    }

    public static Result newStudent() {
        return ok(details.render(studentForm));
    }

    public static Result details(Student student) {
        if (student == null) {
            return notFound(String.format("Student %s does not exist.", student.mssv));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(filledForm));
    }

    public static Result delete(String mssv) {
        Student student = Student.findByMSSV(mssv);
        if(student == null) {
            return notFound(String.format("Student %s does not exists.",mssv));
        }
        for (StockItem stockItem : student.stockItems) {
            stockItem.delete();
        }
        student.delete();
        return redirect(routes.Students.list(0));
    }
    public static Result save() {
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Bạn cần điền đầy đủ thông tin .");
            return badRequest(details.render(boundForm));
        }
        Student student = boundForm.get();

        //kiem tra ten co dung khong/////////////////////////// neu ki ti dac bien bao loi
        int dem1=0;
        for (int i=0;i<student.ten.length();i++){
            if((student.ten.charAt(i)>64)&&(student.ten.charAt(i)<91)||
                    ((student.ten.charAt(i)>96)&&(student.ten.charAt(i)<123)||(student.ten.charAt(i)==' ')))
            {
                dem1 +=0;
            }
            else{
                dem1 +=1;
            }
        }
        if (dem1!=0){
            flash("error","Tên nhập vào có kí tự đặc biệt hoặc số");
            return badRequest(details.render(boundForm));
        }



        //kiem tra dia chi email co hop le ko;////////////////////////////
        int dem=0;
        for (int i=0;i<student.email.length();i++){
            if(student.email.charAt(i)=='@'){
                if(student.email.length()-1==i) {
                    flash("error","email chưa có phần đứng sau @");
                    return badRequest(details.render(boundForm));
                }
                dem =dem +1;
            }
            else if((student.email.charAt(i)<46)&&(student.email.charAt(i)==47)&& (student.email.charAt(i)>57)&&(student.email.charAt(i)<65)
            &&(student.email.charAt(i)>90)&&(student.email.charAt(i)<95)&&(student.email.charAt(i)>122))//neu bang ki tu dac bien khac thi +1;
            {
                dem =dem +1;
            }
        }
        if (dem !=1){
            flash("error","email nhập vào có kí tự đặc biệt");
            return badRequest(details.render(boundForm));
        }

        // kiem tra ma sinh vien

        if(student.mssv.length() !=4){
            flash("error","mã sinh viên phải có độ dài là 4");
            return badRequest(details.render(boundForm));
        }
        for (int i=0;i<student.mssv.length();i++){
            if ((student.mssv.charAt(i)<48)||(student.mssv.charAt(i)>59)){
                flash("error","mã sinh viên phải là chữ số");
                return badRequest(details.render(boundForm));
            }

        }

        if(student.id == null) {
            student.pass=student.mssv;
            //SendPassStudent.sendpass(student.email,student.pass);
            if (Student.findByEmail(student.email) != null) {
                flash("error", "Email này đã được phép sử dụng");
                return badRequest(details.render(boundForm));
            }
            if (Student.findByMSSV(student.mssv) !=null){
                flash("error","MSSV nay khong chinh xac");
                return badRequest(details.render(boundForm));
            }
            student.save();
        }else {
            student.update();
        }

        StockItem stockItem = new StockItem();
        stockItem.student = student;
        stockItem.quantity = 0L;

        stockItem.save();
        Ebean.save(student);

        flash("success", String.format("Successfully added student %s", student));
        return redirect(routes.Students.list(0));
    }
}

